package com.example.springreactive;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class SpringReactiveApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SpringReactiveApplication.class);

    public static List<String> lstNombres = new ArrayList<>();

    public static void main(String[] args) {

        lstNombres.add("Genesis");
        lstNombres.add("Full Power");
        lstNombres.add("Tatiana");
        lstNombres.add("Marcela");
        lstNombres.add("Michael");

        SpringApplication.run(SpringReactiveApplication.class, args);
    }

    //Flujo de datos para un elemento
    public void crearMono() {
        Mono<String> monoString = Mono.just("Genesis");
        monoString.subscribe(x -> log.info("Hola mundo : " + x));
    }

    //Flujo de datos para una lista de elementos
    public void crearFlux() {
        Flux<String> lstFlux = Flux.fromIterable(lstNombres);
        lstFlux.subscribe(mc -> log.info(mc));

        lstFlux.collectList().subscribe(mc -> log.info(mc.toString()));
    }

    // 1 se ejecuta cada vez que haya una interaccion con los
    // elementos
    public void onNext() {
        Flux<String> lstFlux = Flux.fromIterable(lstNombres);
        lstFlux.doOnNext(mc -> log.info("onNext : " + mc)).subscribe();
    }

    // 2 USO DEL MAP, SIRVE PARA TRANSFORMAR UN OBJETO
    public void map() {
        Flux<String> lstFlux = Flux.fromIterable(lstNombres);
        lstFlux.map(mc -> "uso map : " + mc).subscribe(mc -> log.info(mc));

        //probando la propiedad de inmutabilidad
        lstFlux.subscribe(mc -> log.info(mc));

        Flux<String> lstFluxModificado = lstFlux.map(mc -> "uso map : " + mc);
        lstFluxModificado.subscribe(mc -> log.info(mc));
    }

    // 3 USO DEL  FLAT MAP, TMB  SE USA PARA TRANSFORMAR UN OBJETO COMO EL MAP
    // PERO LA DIFERENCIA ES QUE OBLIGA A QUE ESE VALOR TRANSFORMADO SE PONGA
    // OTRO FLUJO DE TIPO MONO
    public void flatMap() {
        Flux<String> lstFlux = Flux.fromIterable(lstNombres);
        lstFlux.flatMap(mc -> Mono.just("uso de flatmap : " + mc))
                .subscribe(mc -> log.info(mc));
    }

    //4 GENERAR RANGO DE NUMEROS
    public void range() {
        Flux<Integer> lstFlux = Flux.range(0, 10);
        lstFlux.subscribe(mc -> log.info(mc.toString()));

        lstFlux.map(mc -> {
            mc = mc * 2;
            return mc + 1;
        }).subscribe(mc -> log.info("N: " + mc));
    }

    // 5 DELAY EN LOS METODOS, SINCRONIZACION DE ELEMENTOS
    public void delayElementos() {
        /*
        Flux<Integer> lstFlux = Flux.range(0, 10)
                .delayElements(Duration.ofSeconds(5))
                .doOnNext(mc -> log.info("delay : " + mc));
        lstFlux.subscribe();  */
        Flux.range(0, 10)
                .delayElements(Duration.ofSeconds(5))
                .doOnNext(mc -> log.info("delay : " + mc))
                .subscribe();
    }

    // 6 ZIP WITH
    public void zipWith() {
        List<String> listaA = new ArrayList<>();
        listaA.add("azul");
        listaA.add("rojo");

        List<String> listaB = new ArrayList<>();
        listaB.add("Genesis");
        listaB.add("Full Power");

        Flux<String> fxA = Flux.fromIterable(listaA);
        Flux<String> fxB = Flux.fromIterable(listaB);

        fxA.zipWith(fxB, (s, s2) -> String.format("Flux1 : %s , Flux2 : %s", s, s2))
                .subscribe(mc -> log.info(mc));

    }

    //7 operador merge para unir listas

    public void merge() {
        List<String> listaA = new ArrayList<>();
        listaA.add("azul");
        listaA.add("rojo");

        List<String> listaB = new ArrayList<>();
        listaB.add("Genesis");
        listaB.add("Full Power");
        listaB.add("Full Power New");

        Flux<String> fxA = Flux.fromIterable(listaA);
        Flux<String> fxB = Flux.fromIterable(listaB);

        Flux.merge(fxA, fxB).subscribe(mc -> log.info(mc));
    }

    // 8 OPERADOR FILTER
    public void filtrar() {
        Flux<String> lst = Flux.fromIterable(lstNombres);
        lst.filter(mc -> mc.startsWith("G")).subscribe(mc -> log.info("filtrar : " + mc));
    }

    //9 TAKE LAST
    public void takelast() {
        Flux<String> lst = Flux.fromIterable(lstNombres);
        lst.takeLast(2).subscribe(mc -> log.info("take last : " + mc));
    }

    //10 TAKE
    public void take() {
        Flux<String> lst = Flux.fromIterable(lstNombres);
        lst.take(2).subscribe(mc -> log.info("take : " + mc));
    }

    //11 DEFAUL EMPTY
    public void defaulEmpty() {
        Flux<String> lst = Flux.fromIterable(lstNombres);
        lst.defaultIfEmpty("lista Vacia").subscribe(mc -> log.info("defaul empy : " + mc));

        Flux<String> lst2 = Flux.fromIterable(new ArrayList<>());
        lst2.defaultIfEmpty("lista Vacia").subscribe(mc -> log.info("defaul empy : " + mc));

    }

    //12 manejando el error
    public void manejoErrorReturn() {
        Flux<String> lst = Flux.fromIterable(lstNombres);
        lst.doOnNext(mc -> {
            throw new ArithmeticException("error lanzado");
        }).onErrorMap(ex -> new Exception(ex.getMessage())).subscribe(mc -> log.info("metodo de error : " + mc));

    }

    //13  REINTENTANDO
    public void reintentando() {
        Flux<String> lst = Flux.fromIterable(lstNombres);

        lst.doOnNext(mc -> {
            log.info("intentando ...");
            throw new ArithmeticException("error lanzado");
        }).retry(5).onErrorReturn("return error").subscribe(mc -> log.info("reintento : " + mc));
        //onErrorReturn es el ultimo lugar donde cae y devuelve el valor
    }

    //14 OPTIONAL

    public void usoDeOptional() {
        //cambio1_2

        String texto = "no deberia de entrar al obtenerString cambio2_v1";
        //String texto = null;
        //var valor = Optional.ofNullable(texto).orElse(obtenerString());
        var valor = Optional.ofNullable(texto).orElseGet(this::obtenerStringGet);
        System.out.println(valor);
        funcionalidadCambio2_v1();
        estoEsOtroMetodoCambio1_v1();
        estoEsOtro02MetodoCambio1_v2();
        estoEsOtro03MetodoCambio1_v2();
        estoEsOtro01MetodoCambio1_v3();
        estoEsOtro01MetodoCambio1_v4();
        estoEsOtro01MetodoCambio1_v5();
        estoEsOtro01MetodoCambio1_v6();

        estoEsOtro01MetodoCambio2_v2();
        estoEsOtro02MetodoCambio2_v2();
        estoEsOtro01MetodoCambio2_v3();
    }

    public String obtenerStringGet() {

        String texto = "no deberia de entrar al obtenerString cambio1-version6";
        //String texto = null;
        var valor = Optional.ofNullable(texto).orElseGet(this::obtenerString);
        System.out.println(valor);

        //
        //agrego metodoDeMaster_v2
        metodoDeMaster_v2();
        return valor;
    }

    public String obtenerString() {
        //cambio-2
        //cambio-1
        String valor = "valor por defecto del cambio1_v6";
        System.out.println("Entro a la funcion obtenerString y tiene el valor " + valor);
        return valor;
    }

    public void estoEsOtroMetodo() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtroMetodoCambio1_v1() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtroMetodoCambio1_v2() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro02MetodoCambio1_v2() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro03MetodoCambio1_v2() {
        //esto es un metodo con una funcionalidad x
    }

    public void metodoDeMaster(){
        //esto es un metodo con una funcionalidad x
    }
    public void funcionalidadCambio2_v1() {
        //funcionalidad de feature 2
    }

    public void metodoDeMaster_v2(){
        //esto es un metodo con una funcionalidad x
    }
    public void estoEsOtroMetodoCambio1_v3() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro01MetodoCambio1_v3() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtroMetodoCambio1_v4() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro01MetodoCambio1_v4() {
        //esto es un metodo con una funcionalidad x
    }


    public void estoEsOtroMetodoCambio2_v2() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro01MetodoCambio2_v2() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro02MetodoCambio2_v2() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtroMetodoCambio1_v5() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro01MetodoCambio1_v5() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtroMetodoCambio1_v6() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro01MetodoCambio1_v6() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtro01MetodoCambio2_v3() {
        //esto es un metodo con una funcionalidad x
    }

    public void estoEsOtroMetodoCambio2_v3() {
        //esto es un metodo con una funcionalidad x
    }


    @Override
    public void run(String... args) throws Exception {
      /*  crearMono();
        crearFlux();
        onNext();
        map();
        flatMap();
        range(); */
        // delayElementos();
     /*   zipWith();
        merge();
        filtrar();
        takelast();
        take();
        defaulEmpty();*/
        // manejoErrorReturn();
        //reintentando();
        usoDeOptional();
        estoEsOtroMetodo();
        //agrego mi metodo estoEsOtroMetodoCambio1_v2
        estoEsOtroMetodoCambio1_v2();

        //agrego mi metodo03Cambio1_v2
        estoEsOtro03MetodoCambio1_v2();
        metodoDeMaster();
        metodoDeMaster_v2();

        //agrego mi metodoCambio1_v3
        estoEsOtroMetodoCambio1_v3();

        //agrego mi metodoCambio1_v4
        estoEsOtroMetodoCambio1_v4();
        estoEsOtroMetodoCambio2_v2();

        //agrego mi metodoCambio1_v5
        estoEsOtroMetodoCambio1_v5();

        //agrego mi metodoCambio1_v6
        estoEsOtroMetodoCambio1_v6();
        estoEsOtroMetodoCambio2_v3();
    }
}
